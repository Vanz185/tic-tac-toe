const { ratingesPlayer } = require('../users')

class RatingController {

    getRatingPlayers(req, res) {
        if (ratingesPlayer){
            return res.status(200).json(ratingesPlayer);
        } else{
            return res.status(404).send('Данные отсутствуют')
        }
            
    }
}

module.exports = new RatingController();