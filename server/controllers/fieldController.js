class FieldController {

    constructor(){
        this.fields = []
    }
    
    
    getField(req, res) {
        const { index } = req.query
        if (this.fields[index] && index){
            return res.status(200).send(this.fields[index])
        } else if (index) {
            return res.status(404).send('Not Found. Данного поля нет')
        } else {
            return res.status(400).send('Bad Request. Неверный синтаксис')
        }
    }

    createField (req, res) {
        const { field } = req.body
        if (field) {
            this.fields[0] = field
            return res.status(200).send('Поле создано')
        } else {
            return res.status(400).send('Bad Request. Данные отсутствуют или неверный синтаксис')
        }
    }

    updateField (req, res) {
        const { field } = req.body
        const { index } = req.query
        if (this.fields[index] && field && index){
            this.fields[index] = field
            return res.status(200).send('Поле перезаписано')
        } else if (field && index){
            return res.status(404).send('Not Found. Данного поля нет')
        } else {
            return res.status(400).send('Bad Request. Неверный синтаксис')
        }
    }

    deleteField (req, res) {
        const { index } = req.query
        if (this.fields[index] && index){
            delete this.fields[index]
            return res.status(200).send('Поле удалено')
        } else if (index) {
            return res.status(404).send('Not Found. Данного поля нет')
        } else {
            return res.status(400).send('Bad Request. Неверный синтаксис')
        }
    }
}

module.exports = new FieldController();