/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('users').del()
  await knex('users').insert([
    {surName: 'Алекссандров', firstName: 'Игнат', secondName: 'Анатолиевич', 
    age: 24, gender: true, statusActive: true, statusGame: false, 
    createdAt: '2023-03-14 15:47:14', updatedAt: '2023-05-02 15:47:14'},
    {surName: 'Мартынов', firstName: 'Остап', secondName: 'Федорович', 
    age: 12, gender: true, statusActive: true, statusGame: true, 
    createdAt: '2023-05-02 15:47:14', updatedAt: '2023-05-02 15:47:14'},
    {surName: 'Комаров', firstName: 'Цефар', secondName: 'Александрович', 
    age: 83, gender: true, statusActive: false, statusGame: false, 
    createdAt: '2023-05-10 15:47:14', updatedAt: '2023-05-10 15:47:14'},
    {surName: 'Нежданова', firstName: 'Марина', secondName: 'Федоровна', 
    age: 20, gender: false, statusActive: true, statusGame: false, 
    createdAt: '2023-05-09 15:47:14', updatedAt: '2023-05-10 15:47:14'},
    {surName: 'Галкин', firstName: 'Феликс', secondName: 'Платонович', 
    age: 25, gender: true, statusActive: true, statusGame: true, 
    createdAt: '2023-05-09 15:47:14', updatedAt: '2023-05-10 15:47:14'},
    {surName: 'Мимикова', firstName: 'Юлия', secondName: 'Андреевна', 
    age: 30, gender: false, statusActive: true, statusGame: false, 
    createdAt: '2022-11-01 15:47:14', updatedAt: '2023-03-15 15:47:14'},
    {surName: 'Марков', firstName: 'Йошка', secondName: 'Федорович', 
    age: 76, gender: true, statusActive: false, statusGame: false, 
    createdAt: '2022-06-21 15:47:14', updatedAt: '2022-12-21 15:47:14'},
    {surName: 'Пупкина', firstName: 'Вероника', secondName: 'Сергеевна', 
    age: 10, gender: false, statusActive: true, statusGame: false, 
    createdAt: '2023-04-03 15:47:14', updatedAt: '2023-05-06 15:47:14'},
    {surName: 'Клинг', firstName: 'Виктория', secondName: 'Ивановна', 
    age: 48, gender: false, statusActive: true, statusGame: true, 
    createdAt: '2023-01-23 15:47:14', updatedAt: '2023-05-10 15:47:14'},
    {surName: 'Шевченко', firstName: 'Рафаил', secondName: 'Михайлович', 
    age: 22, gender: true, statusActive: false, statusGame: false, 
    createdAt: '2021-08-10 15:47:14', updatedAt: '2023-05-02 15:47:14'},

  ]);
};
