const express = require('express');
const fieldController = require('../controllers/fieldController');
const router = express.Router();

router.get('/gameField', fieldController.getField.bind(fieldController))
router.post('/gameField', fieldController.createField.bind(fieldController))
router.delete('/gameField', fieldController.deleteField.bind(fieldController))
router.put('/gameField', fieldController.updateField.bind(fieldController))

module.exports = router;