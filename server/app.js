const express = require('express');
const authRouter = require('./routes/auth')
const ratingRouter = require('./routes/rating')
const gameFieldRouter = require('./routes/gameField')
const PORT = process.env.PORT || 3001
const app = express();
const cors = require('cors')

app.use(cors())
app.use(express.urlencoded());
app.use(express.json());




app.use(authRouter);
app.use(gameFieldRouter);
app.use(ratingRouter);

app.get('/', (req, res) => {
    res.redirect('auth')
})


app.listen(PORT);