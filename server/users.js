const ratingesPlayer = [
    {
        id: 1,
        fio:'Александров Игнат Анатолиевич',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 2,
        fio:'Шевченко Рафаил Михайлович',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 3,
        fio:'Мазайло Трофим Артёмович',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 4,
        fio:'Логинов Остин Данилович',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 5,
        fio:'Борисов Йошка Васильевич',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 6,
        fio:'Соловьёв Ждан Михайлович',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 7,
        fio:'Негода Михаил Эдуардович',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 8,
        fio:'Гордеев Шамиль Леонидович',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 9,
        fio:'Многогрешный Павел Виталиевич',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 10,
        fio:'Александров Игнат Анатолиевич',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 11,
        fio:'Волков Эрик Алексеевич',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 12,
        fio:'Кузьмин Ростислав Васильевич',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 13,
        fio:'Стрелков Филипп Борисович',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    },
    {
        id: 14,
        fio:'Галкин Феликс Платонович',
        countGames:24534,
        countWins:9824,
        countDefeat:1222,
        percentageWins:'87%'
    }
]

module.exports = {
    ratingesPlayer
}