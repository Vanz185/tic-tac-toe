import Auth from "../pages/Auth";
import ConditionsPlayers from "../pages/ConditionsPlayers";
import GameField from "../pages/GameField";
import GameHistory from "../pages/GameHistory";
import ListOfPlayers from "../pages/ListOfPlayers";
import RatingField from "../pages/RatingField";



export const privateRoutes = [
    {path: '/gameField', element: <GameField/>, exact: true},
    {path: '/gameHistory', element: <GameHistory/>, exact: true},
    {path: '/activePlayers', element: <ConditionsPlayers/>, exact: true},
    {path: '/listOfPlayers', element: <ListOfPlayers/>, exact: true},
    {path: '/ratingField', element: <RatingField/>, exact: true},
]

export const publicRoutes =[
    {path: '/auth', element: <Auth/>, exact: true},
]