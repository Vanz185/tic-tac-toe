import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

class Game {

    mode = 'x';
    field = [null, null, null, null, null, null, null, null, null];
    winComb = null;
    users = '';
    
    constructor() {
        makeAutoObservable(this);
        makePersistable(this, {
            name: 'GameStore',
            properties: ['field', 'mode', 'winComb', 'users'],
            storage: window.localStorage,
            expireIn: 86400000,
        })
    };
    
    get winner() {
        return this.isWinner(this.field);
    };

    setMode (value) {
        this.mode = value
    }

    setField (value, i){
        
        this.field[i] = value
    }

    
    get isOver() {
        return this.winner || !this.field.some((cell) => !cell);
    };

    makeMove(index) {
        if (this.field[index]) return alert('Клетка занята');
        this.setField(this.mode, index);
        this.setMode((this.mode === 'x') ? 'o' : 'x')
    };

    moveMade() {
        setTimeout(() => {
            let index = this.field.indexOf(null)
            this.setField(this.mode, index);
            this.setMode((this.mode === 'x') ? 'o' : 'x')
        }, 1000);
    };

    isWinner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
          ];
          for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
              this.winComb = [squares[a], [a, b, c]]
              return true;
            }
          }
        if (squares.includes(null) === false) {
            this.winComb = 'end'
            return true
        }
        return false
    };

    newGame() {
        this.field = [null, null, null, null, null, null, null, null, null]
        this.setMode('x')
        this.winComb = null
        localStorage.setItem('chatFront', '')
        localStorage.setItem('chat', '')
    }

    setUsers(data) {
        this.users = data
    }

    styleBack (index) {
        if (this.winComb !== null && this.winComb !=='end') {                                    // проверка на наличие победителя
            if (this.winComb[1].includes(index)) {                        
              if (this.winComb[0] === 'x')        
                return 'winSquareX';
              else
                return 'winSquareO';
            }
          }
        return 'cell';
    };
    
};

export default Game = new Game();