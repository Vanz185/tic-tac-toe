import { action, computed, makeAutoObservable, observable } from "mobx";
import { makePersistable, stopPersisting } from "mobx-persist-store";
import Game from "./Game";


class TimerField {
    seconds = 0;
    minutes = 0;
    constructor() {
        makeAutoObservable(this);

        makePersistable(this, {
            name: 'Timer',
            properties: ['seconds', 'minutes'],
            storage: window.localStorage,
            expireIn: 86400000,
        })
    };

    updateTime() {
      if (Game.users) {
        this.seconds++;
        if (this.seconds === 60) {
          this.minutes++;
          this.seconds = 0;
        }
        if (this.minutes === 60) {
          this.hours++;
          this.minutes = 0;
        }
      }
      
    }

    resetTimer() {
        this.seconds = 0;
        this.minutes = 0;
    }

};

export default TimerField = new TimerField();