import { action, computed, makeAutoObservable, observable } from "mobx";
import { makePersistable, stopPersisting } from "mobx-persist-store";
import Game from "./Game";
import MokChat from "../MokChat";


class ChatStore {
    
    listMessage = []
    constructor() {
        makeAutoObservable(this);
        makePersistable(this, {
            name: 'ChatFront',
            properties: ['listMessage'],
            storage: window.localStorage,
            expireIn: 86400000,
        })


    };

    get modePlayer(){
        return Game.mode
    }

    
    

    setListMessage (){
        this.listMessage = JSON.parse(localStorage.getItem('chatFront'))
    }

    addNewMessage (message) {
        if (!Object.values(message).some(e => e === '') && Game.users){
            MokChat.sendMessage(Game.users[0].idInGame,message)
            MokChat.messageSent()
            this.setListMessage()
            this._rerunJob()
        }
    }


    _rerunJob() {
        setTimeout(() => {
            MokChat.sendMessage(2,'Ты тут?')
            MokChat.messageSent()
            this.setListMessage()
        }, 3000);
        
    };
    

    resetListMessage() {
        this.listMessage = []
    }
    

};

export default ChatStore = new ChatStore();