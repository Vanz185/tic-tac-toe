import React, { useState } from 'react';
import motionX from '../UI/icons/x.svg'
import motionO from '../UI/icons/zero.svg'
import { observer } from 'mobx-react-lite';
import Game from '../store/Game';

const GameStep = () => {
    
    return (  
        Game.users ?
        <div id="game-step">Ходит&nbsp;<img className="motion" 
        src={Game.mode === 'x' && Game.users ? motionX: motionO} alt=''/>&nbsp;<p>
            {Game.mode === 'x' && Game.users ? Game.users[0].fio.split(" ").reverse().slice(1).join(" "): Game.users[1].fio.split(" ").reverse().slice(1).join(" ")}    
        </p></div>
        :
        <div id="game-step">Нет игроков</div>
    );
}
 
export default observer(GameStep);