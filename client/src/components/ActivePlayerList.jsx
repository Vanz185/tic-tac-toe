import React, { useContext, useState } from 'react';
import { AuthContext } from '../App';
import { useNavigate } from 'react-router-dom';
import ActivePlayerBtn from '../UI/button/ActivePlayerBtn';
import Game from '../store/Game';

const ActivePlayersList = ({statusPlayers}) => {

    
    const navigate = useNavigate();
    
    const chooseOponent =(dateOponent) =>{
        localStorage.setItem('dateOponent', '')
        localStorage.setItem('dateOponent', JSON.stringify(dateOponent))
        let players = [{...JSON.parse(localStorage.getItem('YourDate')), idInGame: 1, mode: 'x'},{...JSON.parse(localStorage.getItem('dateOponent')), idInGame: 2, mode:'o'}]
        Game.setUsers(players)
        localStorage.setItem('chat','')
        navigate('/gameField')
    }

    return (  
        <>
        {statusPlayers.map(statusPlayer =>
            <div className="rowActive" key={statusPlayer.id}>
                <div className="row-name">
                    {statusPlayer.fio}
                </div>
                <div className="rightActive">
                    <div className={statusPlayer.status.class}> {statusPlayer.status.text}</div>
                    <ActivePlayerBtn disabled = {!statusPlayer.status.active} onClick={()=> chooseOponent(statusPlayer)}>Позвать играть</ActivePlayerBtn>
                </div>
            </div>
        )}
        </>
    );
}
 
export default ActivePlayersList;