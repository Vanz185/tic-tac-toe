import React, { useEffect, useState } from 'react';
import MyInput from '../UI/input/MyInput';
import MyButton from '../UI/button/MyButton';
import exitModal from '../UI/icons/exitModal.svg'
import women from '../UI/icons/women.svg'
import man from '../UI/icons/man.svg'

const ModalCreatePlayer = ({create, status, exit, date}) => {
    
    
    const [player, setPlayer] = useState({fio: '', age: '', gender: '', status:status, create: date, edite: date})
    const [disableBtn, setDisableBtn] = useState(false)

    

    const addNewPlayer = (e) => {
        e.preventDefault()
        
        const newPlayer = {
            ...player
        }
        console.log(newPlayer)
        create(newPlayer)
        setPlayer({fio: '', age: '', gender: '', status:status, create: date, edite: date})

        
        
    }

    const btnExit = (e) => {
        e.preventDefault()
        exit()
    }

    useEffect(() =>{
        if (player.fio !=='' && player.age !=='' && player.gender !=='') setDisableBtn(false)
        else setDisableBtn(true)
    }, [player]);
    
    return (  
        <form className='formModalListOfPlayer'>
            <div className="exit-panel">
                <button onClick={e =>btnExit(e)}><img src={exitModal} alt=""/></button>
            </div>
            <h1>Добавьте игрока</h1>
            <div className="fio-set">
                <p>ФИО</p>
                <MyInput 
                    value={player.fio}
                    onChange={e => setPlayer({...player, fio: e.target.value})}
                    type="text" 
                    placeholder="Иванов Иван Иванович"
                />
            </div>
            <div className="age-gender-set">
                <div className="age-set">
                    <p>Возраст</p>
                    <MyInput 
                        value={player.age}
                        onChange={e => setPlayer({...player, age: e.target.value})}
                        type="text" 
                        placeholder="0"
                    />
                </div>
                <div className="gender-set">
                    <p>Пол</p>
                    <div className="gender">
                        <label className='women'>
                            <input
                                type="radio"
                                name="gender"
                                value="women"
                                id="women"
                                onChange={e => setPlayer({...player, gender: e.target.defaultValue})}
                            />
                            <img src={women} alt="" />
                        </label>
                        <label className='man'>
                            <input
                                type="radio"
                                name="gender"
                                value="man"
                                id="man"
                                onChange={e => setPlayer({...player, gender: e.target.defaultValue})}
                            />
                            <img src={man} alt="" />
                        </label>
                    </div>
                </div>
            </div>
            <div className="add-player">
                <MyButton 
                    disabled = {disableBtn}
                    onClick={addNewPlayer}
                >
                    Добавить
                </MyButton>
            </div>


        </form>
    );
}
 
export default ModalCreatePlayer;