import React, { useContext } from 'react';
import {Route, Routes, Navigate} from 'react-router-dom';
import {privateRoutes, publicRoutes} from '../router'
import { AuthContext } from '../App';

const AppRouter = () => {
    
    const {isAuth} = useContext(AuthContext)
    
    const getInSystem = localStorage.getItem('InSystem')

    return (  
      getInSystem || isAuth
      ?
      <Routes>
        {privateRoutes.map(route =>
          <Route
              element={route.element}
              path={route.path}
              exact={route.exact}
              key={route.path}
          />
        )}
        <Route path='*'element={<Navigate to='/activePlayers'/>}/>
      </Routes>
      :
      <Routes>
        {publicRoutes.map(route =>
          <Route
              element={route.element}
              path={route.path}
              exact={route.exact}
              key={route.path}
          />
        )}
        <Route path='*'element={<Navigate to='/auth'/>}/>
      </Routes>
      
        
    );
}
 
export default AppRouter;