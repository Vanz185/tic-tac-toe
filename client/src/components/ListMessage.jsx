import { observer } from 'mobx-react-lite';
import React from 'react';
import ChatStore from '../store/ChatStore';
import Game from '../store/Game';


const ListMessage = () => {
    
    
    return ( 
        <>
        {ChatStore.listMessage.length === 0 ?
            <div className='noMessage'> Сообщений нет</div>
        :
        ChatStore.listMessage.map((message, index) =>
            
            message.mode === 'x'
            ? 
            <div className="msg-container me" key={index}>
                <div className="msg-header">
                        <div className="subject-name x">{Game.users[0].fio.split(" ").reverse().slice(1).reverse().join(" ")}</div>
                        <div className="time">{message.time}</div>
                </div>
                <div className="msg-body">{message.text}</div>
            </div>
            :
            <div className="msg-container other" key={index}>
                <div className="msg-header">
                    <div className="subject-name zero">{Game.users[1].fio.split(" ").reverse().slice(1).reverse().join(" ")}</div>
                    <div className="time">{message.time}</div>
                </div>
                <div className="msg-body">{message.text}</div>
            </div>
        )}
        </>

    );
}
 
export default observer(ListMessage);