import React from 'react';

import smallX from '../UI/icons/smallX.svg'
import smallZero from '../UI/icons/smallRero.svg'
import winner from '../UI/icons/winner.svg'

const HistoryList = ({playersHistory}) => {
    
    return ( 
        <>
        {playersHistory.map((playerHistory, index) =>
            <tr className="history-data" key={index}>
                <td className="game-results">
                    <div className="player-one">
                        <img src={smallZero} alt=''/> 
                        {playerHistory.fioO}
                        {playerHistory.winO ?(
                        <img src={winner} alt=''/>
                        ):(
                            <img alt=''/>
                        )}
                    </div>
                    <div className="against">
                        против
                    </div>
                    <div className="player-two">
                        <img src={smallX} alt=''/> 
                        {playerHistory.fioX}
                        {!playerHistory.winO ?(
                        <img src={winner} alt=''/>
                        ):(
                            <img alt=''/>
                        )}
                    </div>
                </td>
                <td>12 февраля 2022</td>
                <td>43 мин 13 сек</td>
            </tr>
        )}
        </>
    );
}
 
export default HistoryList;