import React from 'react';
import women from '../UI/icons/women.svg'
import man from '../UI/icons/man.svg'
import block from '../UI/icons/block.svg'
import BlockButton from '../UI/button/BlockButton';

const PlayerList = ({playersList, editActive}) => {
    
    const numPlayer = (index, act) =>{
        let reverseStatus
        act.active
        ?
        reverseStatus = 1
        :
        reverseStatus = 0
        editActive(index, reverseStatus)
    }
    
    return ( 
        <>
            {playersList.map((playerList, index) =>
                <tr className="player-list" key={index}>
                    <td>{playerList.fio}</td>
                    <td>{playerList.age}</td>
                    <td>
                        {playerList.gender === 'women'
                        ?
                        <img src={women} alt=''/>
                        :
                        <img src={man} alt=''/>
                        }
                    </td>
                    <td>
                        <div className={playerList.status.className}>{playerList.status.text}</div>
                    </td>
                    <td>{playerList.create}</td>
                    <td>{playerList.edite}</td>
                    <td>
                        <BlockButton onClick={() => numPlayer(index, playerList.status)}>
                            {playerList.status.active
                            ?
                            <>
                                <img src ={block} alt=''/>
                                Заблокировать
                            </>
                            :
                            <>
                                Разблокировать
                            </>
                            }
                        </BlockButton>
                    </td>
                </tr>
            )}
        </>    
    );
}
 
export default PlayerList;