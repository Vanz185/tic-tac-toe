import React from 'react';
import bigWin from '../UI/icons/bigWin.svg'
import MyButton from '../UI/button/MyButton';
import BlockButton from '../UI/button/BlockButton';
import { observer } from 'mobx-react-lite';
import Game from '../store/Game';

const InWinModal = ({startNew, outGame}) => {
    
    
    let winGender = '';

    if (Game.winComb !==null){
        if (Game.winComb[0] === 'x') {
            if (Game.users[0].gender === 'man') winGender = ' победил!'
            else winGender = ' победила!'
        }
        if (Game.winComb[0] === 'o'){
            if (Game.users[1].gender === 'man') winGender = ' победил!'
            else winGender = ' победила!'
        }
    }
    
    
    
    
    return ( 
        <>
        {Game.winComb !== 'end' && Game.winComb !==null &&
			<div className='modal-container'>
                <img src={bigWin} alt="" />
                <p className='name-win'>
                    { Game.winComb[0] === 'x'? Game.users[0].fio: Game.users[1].fio}{winGender}</p>
                <div className='btn-modalWin'>
                    <MyButton className='btn-goNewGame' onClick={startNew}>Новая игра</MyButton>
                    <BlockButton className='btn-goMenu' onClick={outGame}>Выйти в меню</BlockButton>
                </div>
			</div>}
		{Game.winComb === 'end' && Game.winComb !==null &&
            <div className='modal-container'>
                <img src={bigWin} alt="" />
                <p className='name-win'>Ничья</p>
                <div className='btn-modalWin'>
                    <MyButton className='btn-goNewGame' onClick={startNew} >Новая игра</MyButton>
                    <BlockButton className='btn-goMenu' onClick={outGame}>Выйти в меню</BlockButton>
                </div>
            </div>}
        
        </>
    );
}
 
export default observer(InWinModal);