import React, { useState } from 'react';

const PlayerData = () => {
    const [ratingPlayeres, setRatingPlayeres] = useState([
        {
            fio:'Александров Игнат Анатолиевич',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Шевченко Рафаил Михайлович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Мазайло Трофим Артёмович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Логинов Остин Данилович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Борисов Йошка Васильевич',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Соловьёв Ждан Михайлович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Негода Михаил Эдуардович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Гордеев Шамиль Леонидович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Многогрешный Павел Виталиевич',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Пупкин Шамиль Рахатович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Волков Эрик Алексеевич',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Кузьмин Ростислав Васильевич',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Стрелков Филипп Борисович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        },
        {
            fio:'Галкин Феликс Платонович',
            countGames:24534,
            countWins:9824,
            countDefeat:1222,
            percentageWins:'87%'
        }
    ])
    
    

    return (  
        <>
            {ratingPlayeres.map(ratingPlayer =>
                <tr className="player-data" key={ratingPlayer.fio}>
                    <td>{ratingPlayer.fio}</td>
                    <td>{ratingPlayer.countGames}</td>
                    <td>{ratingPlayer.countWins}</td>
                    <td>{ratingPlayer.countDefeat}</td>
                    <td>{ratingPlayer.percentageWins}</td>
                </tr>
            )}
        </>
        
        
    );
}
 
export default PlayerData;