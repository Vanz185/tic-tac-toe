import React from 'react';
import xxlX from "../UI/icons/xxl-x.svg"
import xxlO from "../UI/icons/xxl-zero.svg"
import { observer } from 'mobx-react-lite';
import Game from '../store/Game';

const Board = () => {
    
    const handleClick = (index) => {
        
        if (Game.users){
            if (Game.mode === 'x'){
                Game.makeMove(index)
                if (!Game.isOver) {
                    Game.moveMade();
                }
            } else {
                alert('Сейчас не ваш ход')
            }
        }
    }

    return (  
        
        <>
        {Game.field.map((square, i) => (
            <div key={i} onClick={() => handleClick(i)} className={`${Game.styleBack(i)}`}>
                {
                    square !== null ?
                    <img src={square === 'x'? xxlX : xxlO} alt="" />
                    :
                    square
                }
                
            </div>
        ))}
        </>
        
    );
}
 
export default observer(Board);