const player = [
    {
        name: 'Владлен Пупкин',
    },
    {
        name: 'Екатерина Плюшкина',
    }   
]

let motionName = player[0].name

const [checkMotionName, setCheckMotionName] = useState(motionName);

let xIsNext = true;

const changeMotion = () =>{
    
   motionName = checkMotionName === player[0].name ? setCheckMotionName(player[1].name) : setCheckMotionName(player[0].name)
    
}



let executed = false;
let cell;
let cellArr;
let motionIcon;
let tableField = [];

const initialization = () =>{

    if (!executed) {
        
        cell = document.querySelectorAll('.cell'); 
        cellArr = [].slice.call(cell);
        motionIcon = document.querySelector('.motion');

        for (let i = 3; i > 0; i--) {
            tableField.push(cellArr.splice(0, Math.ceil(cellArr.length / i)));
        }

        executed = true;
        
    }
}





let state = [           
    ["", "", ""],
    ["", "", ""],
    ["", "", ""]
];
let mode = "x";         
let isOverGame = false; 
let isDrawnGame = false; 

const getGameFieldStatus = () => {  //метод для получения информации о состоянии игрового поля
    

    let srtX = [0, 0, 0];   // количество заполнений в каждой строке х

    let srtO = [0, 0, 0];   // количество заполнений в каждой строке y
    
    let stX = [0, 0, 0];    // количество заполнений в каждом столбце x

    let stO = [0, 0, 0];    // количество заполнений в каждом столбце y

        
    let dx1 = 0;            // количество заполнений в 1 диагонали x
    let dx2 = 0;            // количество заполнений во 2 диагонали x

    let do1 = 0;            // количество заполнений в 1 диагонали y         
    let do2 = 0;            // количество заполнений во 2 диагонали y
    
    let empty = 9;          // количество пустых полей
    
    for (let i = 0 ; i < 3; i++){       //подсчет заполнений на поле
        for (let j = 0; j < 3; j++){
            if (state[i][j] === 'x'){  
                srtX[i]++;
                stX[j]++;
                empty -= 1;
                if (i === j)
                {
                    dx1++;
                }
                if (i + j === 2)
                {
                    dx2++;
                }
            }
            if (state[i][j] === 'o'){  
                srtO[i]++;
                stO[j]++;
                empty -= 1;
                if (i === j)
                {
                    do1++;
                }
                if (i + j === 2)
                {
                    do2++;
                }
            }
            
        }
        
    }
    
    const victory = (arr) => {

        for (let i = 0; i < 3; i++){
            if (arr[i] === 3){
                return true;
            }
        }
    }
    

    if (victory(srtX) || victory(stX) || dx1 === 3 || dx2 === 3){         //проверка х на победу
        isOverGame = true;
    } else if (victory(srtO) || victory(stO) || do1 === 3 || do2 === 3){  //проверка o на победу
        isOverGame = true;
    } 
    if (empty <= 0){                                                    //проверка на ничью
        
        isDrawnGame = true;
    }

    
    if (isOverGame){
        console.log(`Победил: ${mode}`)
        
    } else if (isDrawnGame){
        console.log("Ничья")
    } 

    

}

const setMode = () =>{          // метод для изменения активного игрока
    mode = mode === "x" ? "o" : "x";
}


const FieldCellValue = (i,j) =>{  // метод для заполнения клетки поле
    
    
    if (mode === "x"){
        let imageX = document.createElement('img');
        imageX.src = xxlX;
        tableField[i][j].appendChild(imageX);
        state[i][j] = mode;

    } else {
        let imageO = document.createElement('img');
        imageO.src = xxlO;
        tableField[i][j].appendChild(imageO);
        state[i][j] = mode;
    }
        
}


const restart = () =>{
    if (isOverGame === true || isDrawnGame === true){
        for (let i = 0; i < 9; i++){
            cell[i].innerHTML = '';
        }
        state =[           
        ["", "", ""],
        ["", "", ""],
        ["", "", ""]
        ];
        isOverGame = false; 
        isDrawnGame = false; 
        setMode('o');
        
    }

}

const gaming = (i,j) =>{
    
    if (tableField[i][j].innerHTML === ''){
        FieldCellValue(i, j);
        getGameFieldStatus();
        setMode();
        //restart();
        
        
        if (mode === 'x'){
            motionIcon.src = motionX;
        } else motionIcon.src = motionO;   

        changeMotion();

        
        
        
    } else console.log('клетка занята')
        
}

const CheckPosition = (number) =>{
    initialization();
    for (let i = 0; i < 3; i++){
        if (number === i){
            
            gaming(0 , i)
        }
        if (number - 3 === i){
            
            gaming(1 , i)
        }
        if (number - 6 === i){
            
            gaming(2 , i)
        }
    }
}