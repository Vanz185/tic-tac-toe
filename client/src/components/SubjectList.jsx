import { observer } from 'mobx-react-lite';
import React from 'react';
import Game from '../store/Game';
import zero from '../UI/icons/zero.svg'
import xsvg from '../UI/icons/x.svg'


const SubjectList = () => {
    return ( 
        <div id="subject-list">
            <h1>Игроки</h1>
            <div className="container">
                    <div className="subject-container">
                        <div><img className="subject-icon" src={zero} alt='' /></div>
                        <div className="subject-info">
                            <div className="subject-name">{Game.users ? Game.users[1].fio : ''}</div>
                            <div className="subject-percent">63% побед</div>
                        </div>
                    </div>
                    <div className="subject-container">
                        <div><img className="subject-icon" src={xsvg} alt='' /></div>
                        <div className="subject-info">
                            <div className="subject-name">{Game.users ? Game.users[0].fio : ''}</div>
                            <div className="subject-percent">23% побед</div>
                    </div>
                </div>
            </div>
        </div>
    );
}
 
export default observer(SubjectList);