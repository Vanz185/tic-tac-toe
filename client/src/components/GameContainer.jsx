import React, { useEffect, useState } from 'react';
import GameStep from './GameStep';
import Board from './Board';
import WinModal from '../UI/Modal/Win/WinModal'
import InWinModal from './InWinModal';
import { useNavigate } from 'react-router-dom';
import Timer from './Timer';
import { observer } from 'mobx-react-lite'
import Game from '../store/Game';
import TimerField from '../store/TimerField';
import ChatStore from '../store/ChatStore';

const GameContainer = () => {
    const [modal, setModal] = useState(false);
    const navigate = useNavigate();


    const startNewGame = () => {
        TimerField.resetTimer();
        Game.newGame();
        ChatStore.resetListMessage();
        setModal(false)
    }

    const outGame =() =>{
        Game.newGame();
        ChatStore.resetListMessage();
        TimerField.resetTimer();
        Game.setUsers('')
        navigate('/activePlayers');
        localStorage.setItem('dateOponent', '')
    }

   
    return (  
        <>
        <div id="game-container">
          <Timer/>
          <div id="game-board">
              <Board/>
          </div>
          <GameStep />
          <WinModal visible = {modal} setVisible={setModal}>
              <InWinModal startNew ={startNewGame} outGame = {outGame}/>
          </WinModal>
        </div>
        </>

    );
}
 
export default observer(GameContainer);