import { observer } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react';
import TimerField from '../store/TimerField';
import Game from '../store/Game';


const Timer = () => {
    const strSec = String(TimerField.seconds);
    const strMin = String(TimerField.minutes);

    useEffect(() => {
        const time = setInterval(() => { TimerField.updateTime() }, 1000);
        if (Game.isOver) return clearInterval(time);
        return () => {
            clearInterval(time);
        };
    }, [Game.isOver]);
    
    return ( 
        
        <div id="game-time">
            <span className="digits">
                {("0" + strMin).slice(-2)}:
            </span>
            <span className="digits">
                {("0" + strSec).slice(-2)}
            </span>
        </div>
    );
}
 
export default observer(Timer);