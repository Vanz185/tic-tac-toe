import React, { useEffect, useRef, useState } from 'react';
import MyInput from '../UI/input/MyInput';
import ListMessage from './ListMessage';
import sendbtn from '../UI/icons/send-btn.svg'
import ChatStore from '../store/ChatStore';
import { observer } from 'mobx-react-lite';

const Chat = () => {
    
    const divRef = useRef(null)
    const [message, setMessage] = useState('');


    


    const scrollView = () => {
        divRef.current.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start'});
    };
  
    useEffect(() => {
        scrollView()
    }, [ChatStore.listMessage]);
    
    const addMessage = () =>{
        ChatStore.addNewMessage(message)
        setMessage('')
    }
    
    return ( 

        <>
        <div id="chat-container">
            <div className='test'>
                <div className="msgs-container">
                    <ListMessage/>
                    <div id='scroll_dawn' ref={divRef}></div>
                </div>
            </div>
            <div className="msg-interactive-elements">
                <MyInput
                    value={message}
                    onChange={e => setMessage(e.target.value)}
                    placeholder="Сообщение..."
                    type="text" />
                <button onClick={addMessage}><img src={sendbtn} alt='' /></button>
            </div>
        </div>
        </>

    );
}
 
export default observer(Chat);