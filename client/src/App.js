import React, { createContext, useState } from 'react';
import {BrowserRouter} from 'react-router-dom'
import AppRouter from './components/AppRouter';
import NavBar from './UI/NavBar/NavBar';
import './styles/App.css'

export const AuthContext = createContext();

function App() {
  const [isAuth, setIsAuth] = useState(false);
  const [isFioOponent, setIsFioOponent] = useState(null)
  const [genderOponent, setGenderOponent] = useState(null)
  return (
    <AuthContext.Provider value={{
      isAuth,
      setIsAuth,
      isFioOponent,
      setIsFioOponent,
      genderOponent,
      setGenderOponent
    }}>
        <BrowserRouter>
          <AppRouter/>
        </BrowserRouter>
    </AuthContext.Provider>
    
  );
}

export default App;
