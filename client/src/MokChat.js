import ChatStore from "./store/ChatStore";
import Game from "./store/Game";

class MockChat {
    _job;
    stop
    constructor(){
        
    }

    sendMessage(id ,message) {
        let modeInGame
        id === 1 ? modeInGame = 'x' : modeInGame = 'o'
        const newMessage = {
            text: message, time: new Date().toLocaleTimeString().slice(0,-3), mode: modeInGame
        }
        this.setListMessage(newMessage)
        

    }

    stopBot(){
        let chat = JSON.parse(localStorage.getItem('chat'))
        if (chat[chat.length - 1].mode === 'x'){
            this._rerunJob();
        }
    }

    messageSent() {
        var frontChat = localStorage.getItem('chat')
        localStorage.setItem('chatFront', frontChat)
    }

    setListMessage(newMessage) {
        
        if (localStorage.getItem('chat') === ''){
            localStorage.setItem('chat', JSON.stringify([newMessage]))
        } else {
            var oldItems = JSON.parse(localStorage.getItem('chat'));
            oldItems.push(newMessage)
            localStorage.setItem('chat', JSON.stringify(oldItems))
        }
        
        
    }

    

    

    
}

export default MockChat = new MockChat()