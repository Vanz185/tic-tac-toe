import { React, useContext } from 'react';
import logo from '../icons/logo.svg'
import signout from '../icons/signout.svg'
import { NavLink } from 'react-router-dom';
import './NavBar.css'
import { AuthContext } from '../../App';

const NavBar = () => {
    const {isAuth, setIsAuth} = useContext(AuthContext)
    
    const exit = () =>{
        localStorage.clear();
        window.location.reload();
    }

    return (  
        
        <header>
            <div className="logo"><img src={logo} alt=''/></div>
            <div className="nav-panel">
                <div><NavLink to='/gameField' className='nav-item'>Игровое поле</NavLink></div>
                <div><NavLink to='/ratingField' className='nav-item'>Рейтинг</NavLink></div>
                <div><NavLink to='/activePlayers' className='nav-item'>Активные игроки</NavLink></div>
                <div><NavLink to='/gameHistory' className='nav-item'>История игр</NavLink></div>
                <div><NavLink to='/listOfPlayers' className='nav-item'>Список игроков</NavLink></div>
            </div>
            <button
                onClick={exit}
            ><img src={signout} alt=''/>
            </button>
        </header>
        
    );
}
 
export default NavBar;