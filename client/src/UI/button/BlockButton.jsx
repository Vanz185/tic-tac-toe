import React from 'react';
import block from './BlockButton.module.css'

const BlockButton = ({children, ...props}) => {
    return (
        
        <button {...props} className={block.myBtn}>
            {children}
        </button>
    );
};

export default BlockButton;
