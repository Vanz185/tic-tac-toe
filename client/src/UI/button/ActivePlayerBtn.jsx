import React from 'react';
import classes from './ActivePlayerBtn.module.css'

const ActivePlayerBtn = ({children, ...props}) => {
    return (  

        <button {...props} className={classes.activeBtn}>
            {children}
        </button>
    );
}
 
export default ActivePlayerBtn;