import React from 'react';
import cl from './CreatePlayer.module.css';

const CreatePlayer = ({children, visible, setVisible}) => {
    
    const rootClasses = [cl.createPlayer]
    
    if (visible) {
        rootClasses.push(cl.active);
    }

    return (  

        <div className={rootClasses.join(' ')} onClick={() => setVisible(false)}>
            <div className={cl.createPanel} onClick={(e) => e.stopPropagation()}>
                {children}
            </div>
        </div>

    );
}
 
export default CreatePlayer;