import React from 'react';
import style from './WinModal.module.css'
import { observer } from 'mobx-react-lite';
import Game from '../../../store/Game';

const WinModal = ({visible, setVisible, children}) => {
    
    const rootClasses = [style.createPlayer]

    if (Game.winComb !== null){
        setVisible(true)
    }
    
    if (visible) {
        rootClasses.push(style.active);
    }

    return (  

        <div className={rootClasses.join(' ')}>
            <div className={style.createPanel} onClick={(e) => e.stopPropagation()}>
                {children}
            </div>
        </div>

    );
}
 
export default observer(WinModal);