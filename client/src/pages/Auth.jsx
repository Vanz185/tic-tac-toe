import { React, useContext, useEffect, useState } from 'react';
import dog from '../UI/icons/dog.svg'
import { AuthContext } from '../App';
import MyInput from '../UI/input/MyInput';
import MyButton from '../UI/button/MyButton';
import bcrypt from 'bcryptjs'

const Auth = () => {
    
    const {isAuth, setIsAuth} = useContext(AuthContext)
    const [authDate, setAuthDate] = useState({login:'',password:''})
    const [error, setError] = useState(false)
    const [disableBtn, setDisableBtn] = useState(false)

    
    
    let user = require('../components/user.json');
    const auth = event =>{
        event.preventDefault();
        if (bcrypt.compareSync(authDate.password, user.password) && authDate.login === user.login) {
            setIsAuth(true);
            localStorage.setItem('InSystem', true)
            localStorage.setItem('YourDate', JSON.stringify(user.date))
            localStorage.setItem('dateOponent', '')
            
        }else setError(true)
    }

    useEffect(()=> {
        if (authDate.login !=='' && authDate.password !=='') setDisableBtn(false)
        else setDisableBtn(true)
        
    }, [authDate]);
    
    

    return (  
        
        <div id="main-auth">
            <div id="auth-model">
                <div id="dog-icon"><img src={dog} alt=""/></div>
                <h1>Войдите в игру</h1>
                <form onSubmit={auth}>
                    <div id="auth-data">
                        <div>
                            {!error 
                            ?
                            <MyInput 
                                value={authDate.login}
                                onChange={e => setAuthDate({...authDate, login: e.target.value})}
                                placeholder="Логин" 
                                type="text"
                            />
                            :
                            <input
                                value={authDate.login}
                                onChange={e => setAuthDate({...authDate, login: e.target.value})}
                                placeholder="Логин" 
                                type="text"
                                className='errorInput'
                            />
                            }
                            {error && <div className='errorInputText'>Неверный логин</div>}
                        </div>
                        <div>
                            {!error
                            ?
                            <MyInput 
                                value={authDate.password}
                                onChange={e => setAuthDate({...authDate, password: e.target.value})}
                                type="password" 
                                placeholder="Пароль"
                            />
                            :
                            <input
                                value={authDate.password}
                                onChange={e => setAuthDate({...authDate, password: e.target.value})}
                                type="password" 
                                placeholder="Пароль"
                                className='errorInput'
                            />
                            }
                            
                            {error && <div className='errorInputText'>Неверный пароль</div>}
                        </div>
                    </div>
                    <div id="auth-button">
                        <MyButton
                            disabled = {disableBtn}
                        >
                            Войти
                        </MyButton>
                    </div>
                </form>
            </div>
        </div>
    );
}
 
export default Auth;