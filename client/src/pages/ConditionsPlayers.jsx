import React, { useState } from 'react';
import NavBar from '../UI/NavBar/NavBar';
import ActivePlayersList from '../components/ActivePlayerList';

const ConditionsPlayers = () => {
    
    const status = [
        {
          text: 'Свободен',
          class: 'p-free',
          active: true
        },
        {
          text: 'В игре',
          class: 'p-play',
          active: false
        },
    ];
    
    const filterStatus = [
        {   
            id: 1,
            fio:'Александров Игнат Анатолиевич',
            status: status[0],
            gender: 'man'
        },
        {   
            id: 2,
            fio:'Шевченко Елизавета Михайловна',
            status: status[0],
            gender: 'women'
        },
        {   
            id: 3,
            fio:'Мазайло Трофим Артёмович',
            status: status[1],
            gender: 'man'
        },
        {
            id: 4,
            fio:'Логинов Остин Данилович',
            status: status[0],
            gender: 'man'
        },
        {
            id: 5,
            fio:'Борисов Йошка Васильевич',
            status: status[0],
            gender: 'man'
        },
        {   
            id: 6,
            fio:'Соловьёв Ждан Михайлович',
            status: status[1],
            gender: 'man'
        },
    ]

    const [statusPlayers, setStatusPlayers] = useState(filterStatus)
    
    const [checked, setChecked] = useState (false);
    

    const sortList = () => {
        console.log(checked)
        
        !checked
        ?
        setStatusPlayers([...statusPlayers].filter(item => item.status.active === !checked))
        :
        setStatusPlayers(filterStatus)
        
        setChecked((prev) => !prev)
    }
    
    return (  
        
        <>
        <NavBar />
        <div id='activePlayers'>
            <div className='activePlayers-container'>
                <div className='activePlayers-header'>
                    <h1 className='title-block'>Активные игроки</h1>
                    <div className='activePlayers-header-container'>
                        <p>Только свободные</p>
                        <label className='switch'>
                            <input 
                                checked={checked} 
                                onChange={sortList}
                                type="checkbox" 
                                className="switch-input"
                            />
                            <span className="switch-slider"></span>
                        </label>
                    </div>
                </div>
                <div className='activePlayers-list'>
                    <ActivePlayersList statusPlayers = {statusPlayers}/>
                </div>
            </div>
        </div>
        </>
        
            
        
    );
}
 
export default ConditionsPlayers;