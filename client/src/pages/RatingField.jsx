import React from 'react';
import NavBar from '../UI/NavBar/NavBar';
import PlayerData from '../components/PlayerData';

const RatingField = () => {
    return (
        
        <><NavBar />
        <div id="main-rating">
            <div id="rating-panel">
                <h1>Рейтинг игроков</h1>
                <table id="player-achievements">
                    <thead>
                        <tr className="info-panel">
                            <td>ФИО</td>
                            <td>Всего игр</td>
                            <td>Победы</td>
                            <td>Проигрыши</td>
                            <td>Процент побед</td>
                        </tr>
                        <PlayerData/>
                    </thead>
                </table>
            </div>
        </div>
        </>
        
            
        
    );
}
 
export default RatingField;