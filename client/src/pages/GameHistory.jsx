import React, { useState } from 'react';
import NavBar from '../UI/NavBar/NavBar';
import HistoryList from '../components/HistoryList';
const GameHistory = () => {
    
    const [playersHistory, setPlayersHistory] = useState([
        {
            fioO:'Александров И.А.',
            fioX:'Соловьёв Ж.М.',
            date:'12 февраля 2022',
            time:'43 мин 13 сек',
            winO: true,
        },
        {
            fioO:'Шевченко Р.М.',
            fioX:'Логинов О.Д.',
            winO: false,
        },
        {
            fioO:'Мазайло Т.А.',
            fioX:'Соловьёв Ж.М.',
            date:'12 февраля 2022',
            time:'43 мин 13 сек',
            winO: false,
        },
        {
            fioO:'Логинов О.Д.',
            fioX:'Шевченко Р.М.',
            date:'12 февраля 2022',
            time:'43 мин 13 сек',
            winO: true,
        },
        {
            fioO:'Борисов Й.В.',
            fioX:'Соловьёв Ж.М.',
            date:'12 февраля 2022',
            time:'43 мин 13 сек',
            winO: false,
        },
        {
            fioO:'Соловьёв Ж.М.',
            fioX:'Логинов О.Д.',
            date:'12 февраля 2022',
            time:'43 мин 13 сек',
            winO: true,
        },
    ])
    
    
    
    
    return (  
        
        <>
        <NavBar/>
        <div className='history'>
            <div className='container-history'>
              <div className='list-history'>
                    <h1 className='title-block'>История игр</h1>
                    <table className='players-history'>
                        <thead>
                            <tr className="info-panel">
                                <td>Игроки</td>
                                <td>Дата</td>
                                <td>Время игры</td>
                            </tr>
                            <HistoryList playersHistory = {playersHistory}/>
                        </thead>    
                    </table>
                </div>
            </div>
        </div>
        </>
        
    );
}
 
export default GameHistory;