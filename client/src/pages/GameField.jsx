import React, { useContext, useState } from 'react';
import NavBar from '../UI/NavBar/NavBar';
import GameContainer  from '../components/GameContainer';
import Chat from '../components/Chat';
import SubjectList from '../components/SubjectList';

const GameField = () => {
    
    return ( 
        <><NavBar />
        <div id="main-container">
            <SubjectList/>
            <GameContainer/>
            <Chat/>
        </div></>
    );
}
 
export default GameField;