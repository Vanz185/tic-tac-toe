import React, { useState } from 'react';
import NavBar from '../UI/NavBar/NavBar';
import PlayerList from '../components/PlayerList';
import MyButton from '../UI/button/MyButton';
import CreatePlayer from '../UI/Modal/CreatePlayer/CreatePlayer';
import ModalCreatePlayer from '../components/ModalCreatePlayer';

const ListOfPlayers = () => {
    
    const [modal, setModal] = useState(false);

    const status = [
        {
          text: 'Активен',
          className: 'active',
          active: true
        },
        {
          text: 'Заблокирован',
          className: 'block',
          active: false
        }
      ];
    
    const [playersList, setPlayersList] = useState([
        {
            fio:'Александров Игнат Анатолиевич',
            age: 24,
            gender:'women',
            status:status[1],
            create:'12 октября 2021',
            edite:'22 октября 2021',
        },
        {
            fio:'Александров Игнат Анатолиевич',
            age: 18,
            gender:'women',
            status:status[0],
            create:'12 октября 2021',
            edite:'22 октября 2021',
        },
        {
            fio:'Александров Игнат Анатолиевич',
            age: 50,
            gender:'man',
            status:status[0],
            create:'12 октября 2021',
            edite:'22 октября 2021',
        },
        {
            fio:'Александров Игнат Анатолиевич',
            age: 24,
            gender:'women',
            status:status[1],
            create:'12 октября 2021',
            edite:'22 октября 2021',
        },
        {
            fio:'Александров Игнат Анатолиевич',
            age: 30,
            gender:'man',
            status:status[1],
            create:'12 октября 2021',
            edite:'22 октября 2021',
        },
        {
            fio:'Александров Игнат Анатолиевич',
            age: 24,
            gender:'man',
            status:status[0],
            create:'12 октября 2021',
            edite:'22 октября 2021',
        },
    ])

    const monthNames = [
        "января", "февраля", "марта", "aпреля", "мая", "июня",
        "июля", "августа", "сентября", "октября", "ноября", "декабря"
    ]
    let nowDate = new Date(); 
    let date =nowDate.getDate()+' '+monthNames[(nowDate.getMonth())] + ' '+nowDate.getFullYear();
    
    const createPlayer = (newPlayer) => {
        setPlayersList([...playersList, newPlayer])
        setModal(false)
    }

    const editActive = (index, reverseStatus) =>{
        const edit = playersList.map((row, i) => {
            if (i === index) {
                return {...row, status: status[reverseStatus], edite: date}
            } else
                return row
        })
        setPlayersList(edit)
    }

    const exit = () =>{
        setModal(false)
    }

    
    return (  
        
        
        <><NavBar />
        <div className="main-listOfPlayers">
            <div className="list-panel">
                <div className="head-list-panel">
                    <h1>Список игроков</h1>
                    <MyButton onClick={() => setModal(true)}>Добавить игрока</MyButton>
                    <CreatePlayer visible={modal} setVisible={setModal}>
                        <ModalCreatePlayer create={createPlayer} status = {status[0]} exit={exit} date={date}/>
                    </CreatePlayer>
                </div>
                <table className="players-list">
                    <thead>
                        <tr className="info-panel-list">
                            <td>ФИО</td>
                            <td>Возраст</td>
                            <td>Пол</td>
                            <td>Статус</td>
                            <td>Создан</td>
                            <td>Изменен</td>
                            <td></td>
                        </tr>
                        <PlayerList playersList = {playersList} editActive={editActive} />
                    </thead>
                </table>

            </div>

        </div>
        </>
        
        
    );
}
 
export default ListOfPlayers;